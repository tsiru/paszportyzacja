Rails.application.routes.draw do
  resources :map_element_locations

  resources :maps

  resources :element_types

  resources :element_connections

  resources :element_aviable_attributes

  resources :element_attributes

  resources :elements

  resources :attributes

  root to: 'elements#index'
end
