# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150101000000) do

  create_table "attribute", force: true do |t|
    t.string  "name",             limit: 45,                 null: false
    t.text    "description"
    t.boolean "multi",                       default: false, null: false
    t.binary  "user_add_aviable",                            null: false
    t.text    "option"
  end

  add_index "attribute", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "element", force: true do |t|
    t.integer "element_type_id",                            null: false
    t.string  "name",            limit: 45
    t.boolean "is_pattern",                 default: false, null: false
  end

  add_index "element", ["element_type_id"], name: "fk_element_element_type1_idx", using: :btree
  add_index "element", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "element_attribute", force: true do |t|
    t.string  "value",        null: false
    t.integer "element_id",   null: false
    t.integer "attribute_id", null: false
  end

  add_index "element_attribute", ["attribute_id"], name: "fk_element_attribute_attribute1_idx", using: :btree
  add_index "element_attribute", ["element_id"], name: "fk_element_attributes_element1_idx", using: :btree
  add_index "element_attribute", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "element_aviable_attribute", force: true do |t|
    t.integer "element_type_id", null: false
    t.integer "attribute_id",    null: false
  end

  add_index "element_aviable_attribute", ["attribute_id"], name: "fk_element_aviable_attribute_attribute1_idx", using: :btree
  add_index "element_aviable_attribute", ["element_type_id"], name: "fk_element_aviable_attribute_element_type1_idx", using: :btree
  add_index "element_aviable_attribute", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "element_connection", force: true do |t|
    t.string  "connection_type", limit: 45
    t.integer "element_id",                 null: false
    t.integer "element_id2",                null: false
    t.text    "parameters"
  end

  add_index "element_connection", ["element_id"], name: "fk_element_connection_element1_idx", using: :btree
  add_index "element_connection", ["element_id2"], name: "fk_element_connection_element2_idx", using: :btree

  create_table "element_type", force: true do |t|
    t.string  "name",        limit: 45,                null: false
    t.text    "description",                           null: false
    t.integer "max_cord1",              default: 1000, null: false
    t.integer "max_cord2",              default: 1000, null: false
    t.integer "max_cord3",              default: 1000, null: false
  end

  add_index "element_type", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "map", force: true do |t|
    t.string  "name",           limit: 45
    t.integer "max_x_cord",                null: false
    t.integer "max_y_cord",                null: false
    t.string  "background_url"
  end

  add_index "map", ["id"], name: "id_UNIQUE", unique: true, using: :btree

  create_table "map_element_location", force: true do |t|
    t.integer "id_parent"
    t.integer "element_id",           null: false
    t.integer "map_id",               null: false
    t.integer "x_map_cord",           null: false
    t.integer "y_map_cord",           null: false
    t.integer "parent_element_cord1"
    t.integer "parent_element_cord2"
    t.integer "parent_element_cord3"
  end

  add_index "map_element_location", ["element_id"], name: "fk_map_element_location_element1_idx", using: :btree
  add_index "map_element_location", ["id"], name: "id_UNIQUE", unique: true, using: :btree
  add_index "map_element_location", ["id_parent"], name: "fk_id_parent_idx", using: :btree
  add_index "map_element_location", ["map_id"], name: "fk_localization_map_idx", using: :btree

end
