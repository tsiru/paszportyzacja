module ApplicationHelper
  def actions_for(model)
    name = model.class.name.underscore
    {
      partial: "#{name.pluralize}/actions_for",
      locals: { name.to_sym => model }
    }
  end

  def bootstrap_class_for(flash_type)
    case
      when :success
        "alert-success"
      when :error
        "alert-danger"
      when :alert
        "alert-warning"
      when :notice
        "alert-info"
      else
        flash_type
    end
  end
end
