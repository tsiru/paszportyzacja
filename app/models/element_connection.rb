class ElementConnection < ActiveRecord::Base
    self.table_name = 'element_connection'

    validates :element, :element2, :connection_type, presence: true

    belongs_to :element,  :class_name => 'Element', :foreign_key => :element_id
    belongs_to :element2, :class_name => 'Element', :foreign_key => :element_id2
end
