class ElementAviableAttribute < ActiveRecord::Base
    self.table_name = 'element_aviable_attribute'


    belongs_to :element_type, :class_name => 'ElementType', :foreign_key => :element_type_id
    belongs_to :attribute_rel, :class_name => 'Attribute', :foreign_key => :attribute_id
end
