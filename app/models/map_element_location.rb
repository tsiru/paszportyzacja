class MapElementLocation < ActiveRecord::Base
    self.table_name = 'map_element_location'

    validates :element, :map, :x_map_cord, :y_map_cord, presence: true

    belongs_to :map, :class_name => 'Map', :foreign_key => :map_id
    belongs_to :map_element_location, :class_name => 'MapElementLocation', :foreign_key => :id_parent
    belongs_to :element, :class_name => 'Element', :foreign_key => :element_id

    has_many :children, class_name: self.name, foreign_key: 'id_parent'
    belongs_to :parent, class_name: self.name, foreign_key: 'id_parent'
end
