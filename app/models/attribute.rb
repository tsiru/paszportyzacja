class Attribute < ActiveRecord::Base
    self.table_name = 'attribute'

    validates :name, presence: true

    has_many :element_attributes, :class_name => 'ElementAttribute'
    has_many :element_aviable_attributes, :class_name => 'ElementAviableAttribute'
end
