class Map < ActiveRecord::Base
    self.table_name = 'map'

    validates :max_x_cord, :max_y_cord, :background_url, :name, presence: true
    has_many :map_element_locations, :class_name => 'MapElementLocation'
end
