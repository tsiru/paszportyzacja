class ElementAttribute < ActiveRecord::Base
    self.table_name = 'element_attribute'


    belongs_to :element, :class_name => 'Element', :foreign_key => :element_id
    belongs_to :attribute_rel, :class_name => 'Attribute', :foreign_key => :attribute_id
end
