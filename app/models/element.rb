class Element < ActiveRecord::Base
    self.table_name = 'element'


    belongs_to :element_type, :class_name => 'ElementType', :foreign_key => :element_type_id
    has_many :element_attributes, :class_name => 'ElementAttribute'
    has_many :element_connections, :class_name => 'ElementConnection'
    has_many :element_connections, :class_name => 'ElementConnection'
    has_many :map_element_locations, :class_name => 'MapElementLocation'
end
