class ElementType < ActiveRecord::Base
    self.table_name = 'element_type'


    has_many :elements, :class_name => 'Element'
    has_many :element_aviable_attributes, :class_name => 'ElementAviableAttribute'
end
