class ElementAviableAttributesController < ApplicationController
  before_action :set_element_aviable_attribute, only: [:show, :edit, :update, :destroy]

  def index
    @element_aviable_attributes = ElementAviableAttribute.all
    respond_with(@element_aviable_attributes)
  end

  def show
    respond_with(@element_aviable_attribute)
  end

  def new
    @element_aviable_attribute = ElementAviableAttribute.new
    respond_with(@element_aviable_attribute)
  end

  def edit
  end

  def create
    @element_aviable_attribute = ElementAviableAttribute.new(element_aviable_attribute_params)
    @element_aviable_attribute.save
    respond_with(@element_aviable_attribute)
  end

  def update
    @element_aviable_attribute.update(element_aviable_attribute_params)
    respond_with(@element_aviable_attribute)
  end

  def destroy
    @element_aviable_attribute.destroy
    respond_with(@element_aviable_attribute)
  end

  private
    def set_element_aviable_attribute
      @element_aviable_attribute = ElementAviableAttribute.find(params[:id])
    end

    def element_aviable_attribute_params
      params.require(:element_aviable_attribute).permit(:element_type_id, :attribute_id)
    end
end
