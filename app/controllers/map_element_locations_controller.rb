class MapElementLocationsController < ApplicationController
  before_action :set_map_element_location, only: [:show, :edit, :update, :destroy]

  def index
    if params[:id_parent].present?
      @parent = MapElementLocation.find params[:id_parent]
      @map_element_locations = MapElementLocation.where(id_parent: params[:id_parent])
    else
      @map_element_locations = MapElementLocation.where(id_parent: nil)
    end
    respond_with(@map_element_locations)
  end

  def show
    respond_with(@map_element_location)
  end

  def new
    @map_element_location = MapElementLocation.new
    respond_with(@map_element_location)
  end

  def edit
  end

  def create
    @map_element_location = MapElementLocation.new(map_element_location_params)
    @map_element_location.save
    respond_with(@map_element_location)
  end

  def update
    @map_element_location.update(map_element_location_params)
    respond_with(@map_element_location)
  end

  def destroy
    @map_element_location.destroy
    respond_with(@map_element_location)
  end

  private
    def set_map_element_location
      @map_element_location = MapElementLocation.find(params[:id])
    end

    def map_element_location_params
      params.require(:map_element_location).permit(:id_parent, :element_id, :map_id, :x_map_cord, :y_map_cord, :parent_element_cord1, :parent_element_cord2, :parent_element_cord3)
    end
end
