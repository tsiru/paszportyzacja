require 'application_responder'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  self.responder = ::ApplicationResponder
  respond_to :html

  private

  def model
    self.class.name.gsub('Controller', '').singularize.constantize
  end

  def model_param
    model.name.underscore
  end
end
