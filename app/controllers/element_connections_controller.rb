class ElementConnectionsController < ApplicationController
  before_action :set_element_connection, only: [:show, :edit, :update, :destroy]

  def index
    @element_connections = ElementConnection.all
    respond_with(@element_connections)
  end

  def show
    respond_with(@element_connection)
  end

  def new
    @element_connection = ElementConnection.new
    respond_with(@element_connection)
  end

  def edit
  end

  def create
    @element_connection = ElementConnection.new(element_connection_params)
    @element_connection.save
    respond_with(@element_connection)
  end

  def update
    @element_connection.update(element_connection_params)
    respond_with(@element_connection)
  end

  def destroy
    @element_connection.destroy
    respond_with(@element_connection)
  end

  private
    def set_element_connection
      @element_connection = ElementConnection.find(params[:id])
    end

    def element_connection_params
      params.require(:element_connection).permit(:connection_type, :element_id, :element_id2, :parameters)
    end
end
