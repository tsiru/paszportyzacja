class ElementAttributesController < ApplicationController
  before_action :set_element_attribute, only: [:show, :edit, :update, :destroy]

  def index
    @element_attributes = ElementAttribute.all
    respond_with(@element_attributes)
  end

  def show
    respond_with(@element_attribute)
  end

  def new
    @element_attribute = ElementAttribute.new
    respond_with(@element_attribute)
  end

  def edit
  end

  def create
    @element_attribute = ElementAttribute.new(element_attribute_params)
    @element_attribute.save
    respond_with(@element_attribute)
  end

  def update
    @element_attribute.update(element_attribute_params)
    respond_with(@element_attribute)
  end

  def destroy
    @element_attribute.destroy
    respond_with(@element_attribute)
  end

  private
    def set_element_attribute
      @element_attribute = ElementAttribute.find(params[:id])
    end

    def element_attribute_params
      params.require(:element_attribute).permit(:value, :element_id, :attribute_id)
    end
end
