class ElementTypesController < ApplicationController
  before_action :set_element_type, only: [:show, :edit, :update, :destroy]

  def index
    @element_types = ElementType.all
    respond_with(@element_types)
  end

  def show
    respond_with(@element_type)
  end

  def new
    @element_type = ElementType.new
    respond_with(@element_type)
  end

  def edit
  end

  def create
    @element_type = ElementType.new(element_type_params)
    @element_type.save
    respond_with(@element_type)
  end

  def update
    @element_type.update(element_type_params)
    respond_with(@element_type)
  end

  def destroy
    @element_type.destroy
    respond_with(@element_type)
  end

  private
    def set_element_type
      @element_type = ElementType.find(params[:id])
    end

    def element_type_params
      params.require(:element_type).permit(:name, :description, :max_cord1, :max_cord2, :max_cord3)
    end
end
