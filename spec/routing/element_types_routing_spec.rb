require "rails_helper"

RSpec.describe ElementTypesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/element_types").to route_to("element_types#index")
    end

    it "routes to #new" do
      expect(:get => "/element_types/new").to route_to("element_types#new")
    end

    it "routes to #show" do
      expect(:get => "/element_types/1").to route_to("element_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/element_types/1/edit").to route_to("element_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/element_types").to route_to("element_types#create")
    end

    it "routes to #update" do
      expect(:put => "/element_types/1").to route_to("element_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/element_types/1").to route_to("element_types#destroy", :id => "1")
    end

  end
end
