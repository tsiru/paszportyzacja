require "rails_helper"

RSpec.describe MapElementLocationsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/map_element_locations").to route_to("map_element_locations#index")
    end

    it "routes to #new" do
      expect(:get => "/map_element_locations/new").to route_to("map_element_locations#new")
    end

    it "routes to #show" do
      expect(:get => "/map_element_locations/1").to route_to("map_element_locations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/map_element_locations/1/edit").to route_to("map_element_locations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/map_element_locations").to route_to("map_element_locations#create")
    end

    it "routes to #update" do
      expect(:put => "/map_element_locations/1").to route_to("map_element_locations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/map_element_locations/1").to route_to("map_element_locations#destroy", :id => "1")
    end

  end
end
