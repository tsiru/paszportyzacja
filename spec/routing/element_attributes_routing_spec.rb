require "rails_helper"

RSpec.describe ElementAttributesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/element_attributes").to route_to("element_attributes#index")
    end

    it "routes to #new" do
      expect(:get => "/element_attributes/new").to route_to("element_attributes#new")
    end

    it "routes to #show" do
      expect(:get => "/element_attributes/1").to route_to("element_attributes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/element_attributes/1/edit").to route_to("element_attributes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/element_attributes").to route_to("element_attributes#create")
    end

    it "routes to #update" do
      expect(:put => "/element_attributes/1").to route_to("element_attributes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/element_attributes/1").to route_to("element_attributes#destroy", :id => "1")
    end

  end
end
