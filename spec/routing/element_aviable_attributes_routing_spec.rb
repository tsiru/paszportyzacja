require "rails_helper"

RSpec.describe ElementAviableAttributesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/element_aviable_attributes").to route_to("element_aviable_attributes#index")
    end

    it "routes to #new" do
      expect(:get => "/element_aviable_attributes/new").to route_to("element_aviable_attributes#new")
    end

    it "routes to #show" do
      expect(:get => "/element_aviable_attributes/1").to route_to("element_aviable_attributes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/element_aviable_attributes/1/edit").to route_to("element_aviable_attributes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/element_aviable_attributes").to route_to("element_aviable_attributes#create")
    end

    it "routes to #update" do
      expect(:put => "/element_aviable_attributes/1").to route_to("element_aviable_attributes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/element_aviable_attributes/1").to route_to("element_aviable_attributes#destroy", :id => "1")
    end

  end
end
