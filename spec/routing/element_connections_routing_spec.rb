require "rails_helper"

RSpec.describe ElementConnectionsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/element_connections").to route_to("element_connections#index")
    end

    it "routes to #new" do
      expect(:get => "/element_connections/new").to route_to("element_connections#new")
    end

    it "routes to #show" do
      expect(:get => "/element_connections/1").to route_to("element_connections#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/element_connections/1/edit").to route_to("element_connections#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/element_connections").to route_to("element_connections#create")
    end

    it "routes to #update" do
      expect(:put => "/element_connections/1").to route_to("element_connections#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/element_connections/1").to route_to("element_connections#destroy", :id => "1")
    end

  end
end
