# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :map do
    name "MyString"
    max_x_cord 1
    max_y_cord 1
    background_url "MyString"
  end
end
