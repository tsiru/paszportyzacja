# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :map_element_location do
    id_parent 1
    element nil
    map nil
    x_map_cord 1
    y_map_cord 1
    parent_element_cord1 1
    parent_element_cord2 1
    parent_element_cord3 1
  end
end
