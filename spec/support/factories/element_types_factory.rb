# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :element_type do
    name "MyString"
    description "MyText"
    max_cord1 1
    max_cord2 1
    max_cord3 1
  end
end
