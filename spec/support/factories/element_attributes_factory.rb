# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :element_attribute do
    value "MyString"
    element nil
    attribute nil
  end
end
