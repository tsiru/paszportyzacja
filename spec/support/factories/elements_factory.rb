# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :element do
    element_type nil
    name "MyString"
    is_pattern false
  end
end
