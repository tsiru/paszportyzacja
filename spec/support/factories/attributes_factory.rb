# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :attribute do
    name "MyString"
    description "MyText"
    multi false
    user_add_aviable ""
    option "MyText"
  end
end
