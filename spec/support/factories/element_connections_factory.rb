# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :element_connection do
    connection_type "MyString"
    element nil
    element_id2 1
    parameters "MyText"
  end
end
